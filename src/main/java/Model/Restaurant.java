package Model;

import Controller.FileWriterr;
import Model.menuitems.CompositeProduct;
import Model.menuitems.MenuItem;

import java.io.*;
import java.util.ArrayList;

public class Restaurant implements IRestaurantProcessing, Serializable, Observable {
    private CompositeProduct menu = new CompositeProduct();
    private ArrayList<Order> orders = new ArrayList<>();
    private FileWriterr fr = new FileWriterr();
    private ArrayList<Observer> observers = new ArrayList();

    /**
     * @pre none
     * @return
     * Returns all orders that were made up until now
     * @post orders is not null
     */
    @Override
    public ArrayList<Order> getOrders(){
        return this.orders;
    }

    /**
     * @pre m must not be null
     * @param m
     * This method adds a new dish to the menu
     */
    @Override
    public void createMenuItem(MenuItem m) {
        menu.addItem(m);
    }

    /**
     * @pre m must not be null
     * @param m
     * Removes a dish from the restaurant's menu
     * @post none
     */
    @Override
    public void deleteMenuItem(MenuItem m) {
        menu.removeItem(m);
    }

    /**
     * @pre index must be an int
     * @pre nume must be a String
     * @pre price must be a double variable
     * @param index
     * @param nume
     * @param price
     * Edits the data of a dish from the menu
     * @post none
     */
    @Override
    public void editMenuItem(int index, String nume, double price) {
        menu.editItem(index,nume,price);
    }

    /**
     * @pre none
     * @return a composite product representing the restaurant's menu
     * @post menu must not be null
     */
    @Override
    public CompositeProduct getMenu(){
        return this.menu;
    }

    /**
     * @pre id must be an int
     * @pre date must a be string
     * @pre table should be an int
     * @pre itemsOrdered must a be a composite product
     * @param id
     * @param date
     * @param table
     * @param itemsOrdered
     * This method creates a new order and inserts it in the hash table, after
     * the insertion it calls the notifyObserves() method
     * @return o
     * @post o must not be null
     */
    @Override
    public Order createNewOrder(int id,String date,int table,CompositeProduct itemsOrdered) {
        Order o = new Order(id,date,table);
        o.insertItem(o, itemsOrdered);
        orders.add(o);
        this.notifyObservers();
        return o;
    }

    /**
     * @pre o should not be null
     * Creates a text file representing the bill for the order o
     * @param o
     * @post none
     */
    @Override
    public void generateBill(Order o) {
        fr.generateBill(o);
    }

    /**
     * @pre o should not be null
     * @param o
     *  Add a given observer from the observer list
     * @post none
     */
    @Override
    public void register(Observer o) {
        this.observers.add(o);
    }

    /**
     * @pre o should not be null
     * @pre the observers list should not be empty
     * @param o
     *  Removes a given observer from the observer list
     * @post none
     */
    @Override
    public void remove(Observer o) {
        if (!observers.isEmpty()) {
            observers.remove(o);
        }
    }

    /**
     * @pre none
     *  This method notifies the obeservers(in this case the chef) when a new orderd is added by the waiter
     * @post none
     */
    @Override
    public void notifyObservers() {
        for(Observer o: observers){
            o.update(orders.get(orders.size()-1));
        }
    }
}
