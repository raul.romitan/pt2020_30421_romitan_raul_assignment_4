package Model;

import java.util.Observable;
import java.util.Observer;

//is notified each time it must cook food that is
//ordered through a waiter
public class Chef implements Observer {
    private String username;
    private String password;

    public Chef(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Chef{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    @Override
    public void update(Observable o, Object arg) {
    }
}
