package Model;
//can create a new order for a table, add elements from the menu, and compute the bill for an order
public class Waiter {
    private String username;
    private String password;

    public Waiter(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Waiter{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
