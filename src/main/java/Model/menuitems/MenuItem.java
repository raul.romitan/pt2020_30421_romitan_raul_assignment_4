package Model.menuitems;

import java.io.Serializable;

public interface MenuItem extends Serializable {
    double calculatePrice();
    String toString();
}
