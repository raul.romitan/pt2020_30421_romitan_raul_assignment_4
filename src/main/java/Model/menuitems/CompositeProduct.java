package Model.menuitems;

import java.util.ArrayList;
import java.util.Collection;

public class CompositeProduct implements MenuItem{
    private ArrayList<MenuItem> items = new ArrayList<>();

    @Override
    public double calculatePrice() {
        float average = 0;
        for (MenuItem m : items
        ) {
            if (average != 0) {
                average += m.calculatePrice();
                average /= 2;
            } else {
                average += m.calculatePrice();
            }

        }
        return average;
    }

    public void addItem(MenuItem m){
        this.items.add(m);
    }

    public void removeItem(MenuItem m){
        this.items.remove(m);
    }

    public void editItem(int index, String nume, double price){
        BaseProduct m = (BaseProduct) items.get(index);
        if(!nume.equals(""))
            m.setName(nume);
        if(price!=0)
            m.setPrice(price);
    }

    public ArrayList<MenuItem> getItems() {
        return items;
    }

    @Override
    public String toString() {
        return "CompositeProduct{" +
                "items=" + items +
                '}';
    }
}
