package Model;

public interface Observer {
    public void update(Order o);
}
