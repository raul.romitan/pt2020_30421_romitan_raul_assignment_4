package Model;

import Model.menuitems.CompositeProduct;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class Order implements Serializable {
    private int orderId;
    private String date;
    private int table;
    private HashMap<Order, CompositeProduct> oItems= new HashMap<Order, CompositeProduct>();

    public Order(int orderId, String date, int table) {
        this.orderId = orderId;
        this.date = date;
        this.table = table;
    }

    public void insertItem(Order o, CompositeProduct m){
        oItems.put(o, m);
    }

    public void deleteItem(Order o, ArrayList<MenuItem> m){
        oItems.remove(o);
    }

    public int getOrderId() {
        return orderId;
    }

    public CompositeProduct getFood(Order o){
        return oItems.get(o);
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getTable() {
        return table;
    }

    public void setTable(int table) {
        this.table = table;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderId == order.orderId &&
                table == order.table;
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, table);
    }

    @Override
    public String toString() {
        return "orderId=" + orderId +
                ", date='" + date + '\'' +
                ", table=" + table;
    }
}
