package Model;

import Model.menuitems.CompositeProduct;
import Model.menuitems.MenuItem;

import java.util.ArrayList;

public interface IRestaurantProcessing {
    void createMenuItem(MenuItem m);
    void deleteMenuItem(MenuItem m);
    void editMenuItem(int index, String nume, double price);
    CompositeProduct getMenu();
    ArrayList<Order> getOrders();

    Order createNewOrder(int id,String date,int table,CompositeProduct itemsOrdered);
    void generateBill(Order o);

}
