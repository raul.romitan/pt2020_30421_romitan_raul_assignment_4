package View;

import Model.IRestaurantProcessing;
import Model.menuitems.BaseProduct;
import Model.menuitems.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdminGUI {

    private static JPanel main = new JPanel();
    private JButton createItem = new JButton("Add menu item");
    private JButton deleteItem = new JButton("Delete menu item");
    private JButton editItem = new JButton("Edit menu item");
    private DefaultListModel  listModel = new DefaultListModel();
    private JList list;
    private IRestaurantProcessing irp;

    public AdminGUI(IRestaurantProcessing irp){
        this.irp=irp;
        JPanel org = new JPanel();
        org.setLayout(new BorderLayout());

        for(MenuItem m : irp.getMenu().getItems()){
            listModel.addElement(m);
        }

        JPanel buttons = new JPanel();

        createItem.setActionCommand("add");
        createItem.addActionListener(new createIListener());
        deleteItem.setActionCommand("delete");
        deleteItem.addActionListener(new deleteIListener());
        editItem.setActionCommand("edit");
        editItem.addActionListener(new editIListener());

        buttons.add(createItem);
        buttons.add(deleteItem);
        buttons.add(editItem);
        org.add(buttons,BorderLayout.NORTH);

        list = new JList(listModel);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setSelectedIndex(0);
        //list.addListSelectionListener((ListSelectionListener) this.list);
        list.setVisibleRowCount(5);
        JScrollPane listScrollPane = new JScrollPane(list);

        org.add(list,BorderLayout.CENTER);
        main.add(org);
    }
    
    public static JPanel getMain(){
        return main;
    }


    class deleteIListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int index = list.getSelectedIndex();
            irp.deleteMenuItem((MenuItem) list.getModel().getElementAt(index));
            listModel.remove(index);
            int size = listModel.getSize();
            if (size == 0) {
                deleteItem.setEnabled(false);
            } else {
                if (index == listModel.getSize()) {
                    index--;
                }
                list.setSelectedIndex(index);
                list.ensureIndexIsVisible(index);
            }
        }
    }

    class createIListener implements ActionListener {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            JTextField name = new JTextField(5);
            JTextField price = new JTextField(5);
            JPanel myPanel = new JPanel();
            myPanel.add(new JLabel("Name:"));
            myPanel.add(name);
            myPanel.add(Box.createHorizontalStrut(30)); // a spacer
            myPanel.add(new JLabel("Price:"));
            myPanel.add(price);
            int result = JOptionPane.showConfirmDialog(null, myPanel,
                    "Create Menu Item", JOptionPane.OK_CANCEL_OPTION);
            if (result == JOptionPane.OK_OPTION) {
                System.out.println("Name: " + name.getText());
                System.out.println("Price: " + price.getText());
                MenuItem m =new BaseProduct(name.getText(),Double.parseDouble(price.getText()));
                irp.createMenuItem(m);
                listModel.addElement(m);
            }
        }
    }

    class editIListener implements ActionListener{
        @Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            int index = list.getSelectedIndex();
            JTextField name = new JTextField(5);
            JTextField price = new JTextField(5);
            JPanel myPanel = new JPanel();
            myPanel.add(new JLabel("Name:"));
            myPanel.add(name);
            myPanel.add(Box.createHorizontalStrut(30)); // a spacer
            myPanel.add(new JLabel("Price:"));
            myPanel.add(price);
            int result = JOptionPane.showConfirmDialog(null, myPanel,
                    "Edit Menu Item", JOptionPane.OK_CANCEL_OPTION);
            if (result == JOptionPane.OK_OPTION) {
                double p=0;
                if(!price.getText().equals(""))
                    p=Double.parseDouble(price.getText());
                irp.editMenuItem(index,name.getText(),p);
                listModel.removeAllElements();
                for(MenuItem m : irp.getMenu().getItems()){
                    listModel.addElement(m);
                }

            }
        }
    }
}
