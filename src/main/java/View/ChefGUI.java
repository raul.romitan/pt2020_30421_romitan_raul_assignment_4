package View;

import Model.*;
import Model.menuitems.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;

public class ChefGUI implements Observer, Serializable {
    private static JPanel main = new JPanel();
    private DefaultListModel  listModel = new DefaultListModel();
    private JList list;
    private Restaurant irp;

    public ChefGUI(Restaurant irp) {
        this.irp=irp;
        this.irp.register(this);
        for(Order o:irp.getOrders()){
            for(MenuItem m : o.getFood(o).getItems()){
                listModel.addElement("Table: "+o.getTable()+", "+m.toString());
            }
        }
        JPanel org = new JPanel();
        org.setLayout(new BorderLayout());
        list = new JList(listModel);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setSelectedIndex(0);
        list.setVisibleRowCount(5);
        JScrollPane listScrollPane = new JScrollPane(list);
        org.add(list,BorderLayout.CENTER);
        main.add(org);
    }

    public static JPanel getMain(){
        return main;
    }


    @Override
    public void update(Order o) {
        for(MenuItem m : o.getFood(o).getItems()){
            listModel.addElement("Table: "+o.getTable()+", "+m.toString());
        }
    }
}
