package View;
import Model.IRestaurantProcessing;
import Model.Order;
import Model.menuitems.CompositeProduct;
import Model.menuitems.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;

public class WaiterGUI{
    private static JPanel main = new JPanel();
    private int ORDERIDS = 0;
    private JButton createBtn = new JButton("Create order");
    private JButton compute = new JButton("Compute bill");
    private JButton oInfo = new JButton("Order info");
    private DefaultListModel  listModel = new DefaultListModel();
    private JList list;
    private DefaultListModel  menuListModel = new DefaultListModel();
    private JList menuList;
    private IRestaurantProcessing irp;


    public WaiterGUI(IRestaurantProcessing irp) {
        this.irp=irp;
        JPanel org = new JPanel();
        org.setLayout(new BorderLayout());

        for(Order o : irp.getOrders()){
            listModel.addElement(o);
        }

        JPanel buttons = new JPanel();

        createBtn.setActionCommand("creates");
        createBtn.addActionListener(new createOListener());
        compute.setActionCommand("computes");
        compute.addActionListener(new computeBListener());
        oInfo.setActionCommand("info");
        oInfo.addActionListener(new oInfoListener());

        buttons.add(createBtn);
        buttons.add(compute);
        buttons.add(oInfo);
        org.add(buttons,BorderLayout.NORTH);

        list = new JList(listModel);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setSelectedIndex(0);
        list.setVisibleRowCount(5);
        JScrollPane listScrollPane = new JScrollPane(list);

        for(MenuItem m : irp.getMenu().getItems()){
            menuListModel.addElement(m);
        }

        menuList = new JList(menuListModel);
        menuList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        menuList.setSelectedIndex(0);
        menuList.setVisibleRowCount(5);
        JScrollPane listScrollPane2 = new JScrollPane(list);
        org.add(list,BorderLayout.CENTER);

        main.add(org);
    }

    public static JPanel getMain(){
        return main;
    }

    class createOListener implements ActionListener {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            menuListModel.removeAllElements();
            for(MenuItem m : irp.getMenu().getItems()){
                menuListModel.addElement(m);
            }
            JTextField tableNr = new JTextField(5);
            JPanel myPanel = new JPanel();
            myPanel.add(new JLabel("Table nr:"));
            myPanel.add(tableNr);
            myPanel.add(menuList);
            int result = JOptionPane.showConfirmDialog(null, myPanel,
                    "Create new order", JOptionPane.OK_CANCEL_OPTION);
            if (result == JOptionPane.OK_OPTION) {
                LocalDateTime now = LocalDateTime.now();
                ORDERIDS++;
                int[] selectedIx = menuList.getSelectedIndices();
                CompositeProduct itemsOrdered = new CompositeProduct();
                for (int ix : selectedIx) {
                    itemsOrdered.addItem((MenuItem) menuList.getModel().getElementAt(ix));
                }
                Order order = irp.createNewOrder(ORDERIDS,now.toString(),Integer.parseInt(tableNr.getText()),itemsOrdered);
                listModel.addElement(order);
                System.out.println(order);
            }
        }
    }

    class oInfoListener implements ActionListener {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            int index = list.getSelectedIndex();
            Order o = (Order) listModel.get(index);
            menuListModel.removeAllElements();
            for(MenuItem m : o.getFood(o).getItems()){
                menuListModel.addElement(m);
            }
            JPanel myPanel = new JPanel();
            myPanel.add(menuList);
            int result = JOptionPane.showConfirmDialog(null, myPanel,
                    "Order Info(The food that was ordered)", JOptionPane.OK_CANCEL_OPTION);
        }
    }

    class computeBListener implements ActionListener{
        @Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            int index = list.getSelectedIndex();
            Order o = (Order) listModel.get(index);
            JPanel myPanel = new JPanel();
            myPanel.add(new JLabel("The bill for table "+o.getTable()+" was generated."));
            irp.generateBill(o);
            int result = JOptionPane.showConfirmDialog(null, myPanel,
                    "Generate Bill", JOptionPane.OK_CANCEL_OPTION);
        }
    }

}
