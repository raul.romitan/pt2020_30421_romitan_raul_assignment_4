package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class MainView extends JFrame {
    private JButton admin = new JButton("Administrator");
    private JButton chef = new JButton("Chef");
    private JButton waiter = new JButton("Waiter");
    private JPanel control = new JPanel();
    private JPanel main = new JPanel();

    public MainView() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize(screenSize.width/2,screenSize.height/2);
        this.validate();

        this.setTitle("Restaurant managemet system by Romitan Raul");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());

        control.setLayout(new GridLayout(1,3));
        control.add(admin);
        control.add(chef);
        control.add(waiter);

        this.add(control,BorderLayout.NORTH);
        main.add(new JLabel("Welcome to my restaurant managing application! You can switch between profiles from the above buttons."));
        this.add(main,BorderLayout.CENTER);
    }

    public void adminAddListener(ActionListener aal){
        admin.addActionListener(aal);
    }
    public void chefAddListener(ActionListener cal){
        chef.addActionListener(cal);
    }
    public void waiterAddListener(ActionListener wal){
        waiter.addActionListener(wal);
    }

    public void refresh(JPanel panel){
        main.removeAll();
        main.add(panel);
        main.revalidate();
        main.repaint();
    }

}
