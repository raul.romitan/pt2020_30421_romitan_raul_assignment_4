package Controller;

import View.AdminGUI;
import View.ChefGUI;
import View.MainView;
import View.WaiterGUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {
    MainView mainView;
    AdminGUI adminView;
    ChefGUI chefView;
    WaiterGUI waiterView;
    public Controller(MainView mainView, AdminGUI adminView, ChefGUI chefView,WaiterGUI waiterView) {
        this.mainView=mainView;
        this.adminView=adminView;
        this.chefView=chefView;
        this.waiterView=waiterView;
        mainView.adminAddListener(new adminListener());
        mainView.chefAddListener(new chefListener());
        mainView.waiterAddListener(new waiterListener());
    }

    class adminListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            mainView.refresh(AdminGUI.getMain());
        }
    }

    class chefListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            mainView.refresh(ChefGUI.getMain());
        }
    }

    class waiterListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            mainView.refresh(WaiterGUI.getMain());
        }
    }
}

