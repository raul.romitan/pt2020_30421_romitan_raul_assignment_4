package Controller;

import Model.Restaurant;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class RestaurantSer {
    public static void serialize(Restaurant r, String fileName)
    {
        try
        {
            FileOutputStream file = new FileOutputStream(fileName);
            ObjectOutputStream outStream = new ObjectOutputStream(file);
            outStream.writeObject(r);
            outStream.close();
            file.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static Restaurant deserialize(String fileName)
    {
        Restaurant r = null;
        try
        {
            FileInputStream file = new FileInputStream(fileName);
            ObjectInputStream inStream = new ObjectInputStream(file);
            r = (Restaurant) inStream.readObject();
            inStream.close();
            file.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return r;
    }


}
