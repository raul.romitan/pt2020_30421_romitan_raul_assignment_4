package Controller;

import Model.Order;
import Model.menuitems.MenuItem;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;

public class FileWriterr implements Serializable {
    public void generateBill(Order o){
        try {
            String fileName = "Bill_for_table_"+o.getTable()+".txt";
            File myObj = new File(fileName);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
            FileWriter myWriter = new FileWriter(fileName);
            myWriter.write("-Bill info-\n");
            myWriter.write("Table: "+o.getTable()+"\n");
            myWriter.write("Date: "+o.getDate()+"\n");
            myWriter.write("Items: \n");
            for(MenuItem m : o.getFood(o).getItems()){
                myWriter.write(m.toString()+"\n");
            }
            myWriter.write("Total: "+o.getFood(o).calculatePrice()+" $");
            myWriter.close();

        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
