package Start;

import Controller.Controller;
import Model.Restaurant;
import Controller.RestaurantSer;
import View.AdminGUI;
import View.ChefGUI;
import View.MainView;
import View.WaiterGUI;

public class Start {
    public static void main(String[] args) {
        Restaurant rdb=new Restaurant();
        rdb= RestaurantSer.deserialize("restaurant.ser");
        MainView mainView = new MainView();
        AdminGUI adminView=new AdminGUI(rdb);
        ChefGUI chefView=new ChefGUI(rdb);
        WaiterGUI waiterView=new WaiterGUI(rdb);
        mainView.setVisible(true);
        Controller controller = new Controller(mainView,adminView,chefView,waiterView);
        Restaurant finalRdb = rdb;
        Runtime.getRuntime().addShutdownHook(new Thread(() -> RestaurantSer.serialize(finalRdb,"restaurant.ser")));
    }
}
